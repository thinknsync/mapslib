package com.thinknsync.mapslib.mapsDrawer;

import com.thinknsync.positionmanager.TrackingData;

import java.util.List;

/**
 * Created by shuaib on 5/20/17.
 */

public interface PathDrawer {
    int LineWidth = 10;
    int LineColor = 0xFFFF0000; //RED

    void setPathProperties(PathProperties pathProperties);
    int drawPath(PathDataManager pathData);
    void drawPath(List<TrackingData> pathData);
    List<Integer> getLinesDrawn();
    int getLastLineDrawn();
    void removeLine(int polyline);
    void removeAllLines();
}
