package com.thinknsync.mapslib.mapsDrawer;

import androidx.annotation.NonNull;

import com.thinknsync.mapslib.TextFormatter;
import com.thinknsync.mapslib.Units;
import com.thinknsync.positionmanager.TrackingData;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by shuaib on 5/25/17.
 */

public class PathDataManagerImpl implements PathDataManager {

    private JSONObject pathData;
    private double totalDistance;
    private double totalTime;
    private Units unitDistance;
    private TimeUnit unitTime;

    public PathDataManagerImpl(JSONObject pathData, Units unitDistance, TimeUnit unitTime) throws JSONException {
        this.pathData = pathData;
        this.totalDistance = getPathDistanceInMeters();
        this.totalTime = getCommuteTimeInSecs();

        if (unitDistance == Units.KM) {
            totalDistance /= 1000;
        }

        if (unitTime == TimeUnit.MINUTES) {
            totalTime /= 60;
        }

        this.unitDistance = unitDistance;
        this.unitTime = unitTime;
    }

    public PathDataManagerImpl(JSONObject pathData) {
        this.pathData = pathData;
    }

    @Override
    public JSONObject getPathData() {
        return pathData;
    }

    @Override
    public void setPathData(JSONObject pathData) {
        this.pathData = pathData;
    }

    @Override
    public double getTotalDistance() {
        return totalDistance;
    }

    @Override
    public Units getUnitDistance() {
        return unitDistance;
    }

    @Override
    public void setUnitDistance(Units unitDistance) {
        this.unitDistance = unitDistance;
    }

    @Override
    public double getTotalTime() {
        return totalTime;
    }

    @Override
    public TimeUnit getUnitTime() {
        return unitTime;
    }

    @Override
    public void setUnitTime(TimeUnit unitTime) {
        this.unitTime = unitTime;
    }

    @Override
    public String getDistanceString() {
        DecimalFormat textFormatter = new TextFormatter().getNumberFormatter1DecimalDigit();
        return textFormatter.format(totalDistance) + " " + unitDistance.getUnitTitle();
    }

    @Override
    public String getTimeString() {
        return new TextFormatter().get2DigitDecimal(totalTime) + " " + unitTime.name();
    }

    @Override
    public List<TrackingData> getPathCoordinates() {
        List<List<HashMap<String, String>>> pathDrawData = new GoogleMapsPathDataParser().parse(getPathData());

        ArrayList<TrackingData> points = new ArrayList<>();
        // Traversing through all the routes
        for (int i = 0; i < pathDrawData.size(); i++) {
            // Fetching i-th route
            List<HashMap<String, String>> path = pathDrawData.get(i);
            // Fetching all the points in i-th route
            points = getPointsFromPaths(path);
        }

        return points;
    }

    @Override
    public double getPathDistanceInMeters() throws JSONException {
        double distance = getPathData().getJSONArray(key_routes).getJSONObject(0).
                getJSONArray(key_legs).getJSONObject(0).getJSONObject(key_distance).getDouble(key_value);

//        return (double) Math.round(distance * 100) / 100;
        return distance;
    }

    @Override
    public int getCommuteTimeInSecs() throws JSONException {
            return getPathData().getJSONArray(key_routes).getJSONObject(0).
                    getJSONArray(key_legs).getJSONObject(0).getJSONObject(key_duration).getInt(key_value);
    }

    private ArrayList<TrackingData> getPointsFromPaths(List<HashMap<String, String>> path) {
        ArrayList<TrackingData> points = new ArrayList<>();

        for (int j = 0; j < path.size(); j++) {
            TrackingData position = getLatLngFromPath(path, j);
            points.add(position);
        }

        return points;
    }

    @NonNull
    private TrackingData getLatLngFromPath(List<HashMap<String, String>> path, int j) {
        HashMap<String, String> point = path.get(j);
        double lat = Double.parseDouble(point.get("lat"));
        double lng = Double.parseDouble(point.get("lng"));
        TrackingData locationData = new TrackingData(lat, lng);
        locationData.setTimeStamp(Calendar.getInstance().getTimeInMillis());
        return locationData;
    }
}
