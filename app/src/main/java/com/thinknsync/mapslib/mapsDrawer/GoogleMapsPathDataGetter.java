package com.thinknsync.mapslib.mapsDrawer;

import com.android.volley.Request;
import com.thinknsync.apilib.ApiDataObjectImpl;
import com.thinknsync.apilib.ApiRequest;
import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.mapslib.Utils;
import com.thinknsync.mapslib.apiCall.GoogelGeoCodeWaypointApiRequestImpl;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.positionmanager.TrackingData;

import java.util.HashMap;

public class GoogleMapsPathDataGetter implements PathDataFetcher {

    private HashMap<String, TrackingData> locationDataHashMap = new HashMap<>();

    private AndroidContextWrapper contextWrapper;
    private NavigationMode navigationMode;

    public GoogleMapsPathDataGetter(AndroidContextWrapper contextWrapper, TrackingData[] sourceAndDestination, NavigationMode navigationMode){
        this.contextWrapper = contextWrapper;
        setSourceAndDestination(sourceAndDestination[0], sourceAndDestination[1]);
        setNavigationMode(navigationMode);
    }

    @Override
    public void setSourceAndDestination(TrackingData sourceLocation, TrackingData destinationLocation) {
        locationDataHashMap.put(keySource, sourceLocation);
        locationDataHashMap.put(keyDestination, destinationLocation);
    }

    @Override
    public void setNavigationMode(NavigationMode navigationMode) {
        this.navigationMode = navigationMode;
    }

    @Override
    public void getRouteData(ApiResponseActions responseActions) {
        ApiRequest apiRequest = getPathDatApiRequest(responseActions);
        apiRequest.sendRequest();
    }

    private ApiRequest getPathDatApiRequest(final ApiResponseActions responseActions){
        ApiRequest apiRequest = new GoogelGeoCodeWaypointApiRequestImpl(contextWrapper,
                getRouteDataUrl(locationDataHashMap.get(PathDataFetcher.keySource), locationDataHashMap.get(PathDataFetcher.keyDestination),
                        navigationMode.getNameString(), Utils.getGoogleApiKey(contextWrapper)), new ApiDataObjectImpl(new HashMap<>()),
                Request.Method.GET, responseActions, 1);

        return apiRequest;
    }

    private String getRouteDataUrl(TrackingData from, TrackingData to, String navigationMode, String apiKey) {
        String strOrigin = "origin=" + from.getLat() + "," + from.getLon();
        String strDest = "destination=" + to.getLat() + "," + to.getLon();
        String mode = "mode=" + navigationMode;
        String output = "json";

        String parameters = strOrigin + "&" + strDest + "&" + mode;
        String key = "&key=" + apiKey;
        return "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + key;
    }
}
