package com.thinknsync.mapslib.mapsDrawer;

import com.thinknsync.apilib.ApiResponseActions;
import com.thinknsync.positionmanager.TrackingData;

/**
 * Created by shuaib on 5/20/17.
 */

public interface PathDataFetcher {
    String keySource = "source";
    String keyDestination = "destination";

    enum  NavigationMode {
        ModeDriving("driving"),
        ModeWalking("walking"),
        ModeCycling("bicycling"),
        ModeTransit( "transit");

        private String name;

        NavigationMode(String name) {
            this.name = name;
        }

        public String getNameString(){
            return this.name;
        }
    }

    void setSourceAndDestination(TrackingData sourceLocation, TrackingData destinationLocation);
    void setNavigationMode(NavigationMode navigationMode);
    void getRouteData(ApiResponseActions responseActions);
}
