package com.thinknsync.mapslib;

import com.thinknsync.mapslib.mapWorks.markers.MarkerInformation;
import com.thinknsync.mapslib.mapsDrawer.PathDataManager;
import com.thinknsync.mapslib.mapsDrawer.PathProperties;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.positionmanager.TrackingData;

import java.util.List;

/**
 * Created by shuaib on 5/19/17.
 */

public interface MapsOperation<T extends Object> {
    String tag = "mapsListener";

    float defaultZoomLevel = 14.0f;
    float maxZoomLevel = 100.0f;
    float minZoomLevel = 5.0f;
    boolean goToCurrentLocationOnMapStart = true;

    void clearMap();

    void setMapView(TrackingData locationData, float zoomLevel);

    void setMapView(TrackingData locationData);

    void setMapView(TrackingData[] locationDataList);

    void setZoomLevel(TrackingData[] locationDataList);

    void setZoomLevel(TrackingData[] locationDataList, int[] paddingLeftTopRightBottom);

    void setZoomLevel(int zoomLevel);

    void setShouldGoToCurrentLocation(boolean shouldGoToCurrentLocation);

    T getMap();

    float getDefaultZoomLevel();

    int drawPath(PathDataManager pathData, PathProperties... pathProperties);

    void drawPath(List<TrackingData> trackingDataList, PathProperties... pathProperties);

    void clearPath(int pathId);

    void clearAllPaths();

    void setupMapMoveStartListener(ObserverHost moveStartObserverHost);

    void setupMapIdleCenterListener(ObserverHost centerCoordinateFetcher);

    void setupDragListener(ObserverHost cameraMoveObserver);

    void setOnMarkerClickAction(final ObserverHost<MarkerInformation> markerClickObserver);

    void setOnMapClickAction(final ObserverHost<TrackingData> clickObserver);

    ObserverHost<TrackingData> getMapClickObserverHost();

    ObserverHost getMapDragObserverHost();

    ObserverHost<MarkerInformation> getMapMarkerClickObserverHost();

    ObserverHost getMapIdleObserverHost();

    ObserverHost getMapMoveStartObserverHost();

    boolean isLocationInScreen(TrackingData trackingData);

    boolean isMapMoving();

    void enableDisableMapInteraction(boolean enableDisable);
}