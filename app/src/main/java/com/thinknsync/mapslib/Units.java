package com.thinknsync.mapslib;

public enum Units {
    KM("KM");

    private final String unit_title;

    Units(String unit_title) {
        this.unit_title = unit_title;
    }

    public String getUnitTitle() {
        return this.unit_title;
    }
}
