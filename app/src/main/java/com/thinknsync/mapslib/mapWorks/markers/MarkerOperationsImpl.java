package com.thinknsync.mapslib.mapWorks.markers;

import android.graphics.Bitmap;
import android.graphics.Point;

import androidx.annotation.Nullable;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.TrackingData;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class MarkerOperationsImpl implements MarkerOperations<GoogleMap> {

    private HashMap<MarkerInformation, Marker> markerHashmap;
    private GoogleMap googleMap;

    public MarkerOperationsImpl(GoogleMap mapObject){
        setMapObject(mapObject);
        markerHashmap = new HashMap<>();
    }

    @Override
    public void setPin(final MarkerInformation markerInformation) {
        LatLng latLon = new LatLng(markerInformation.getTrackingData().getLat(), markerInformation.getTrackingData().getLon());
        MarkerOptions markerOptions = new MarkerOptions().position(latLon).title(markerInformation.getTitle());
        Bitmap markerBitmap = (Bitmap) markerInformation.getActiveDrawable().getDrawableForMarker().getFrameworkObject();
        if (markerBitmap != null) {
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(markerBitmap));
        }
        Marker marker = googleMap.addMarker(markerOptions);
        marker.setTag(markerInformation.getId());

        if(markerInformation.getOnclickInfoDialog() != null) {
            markerInformation.getOnclickInfoDialog().onDismiss(new TypedObserverImpl() {
                @Override
                public void update(Object o) {
                    onMarkerUnSelected(markerInformation);
                }
            });
        }
        removeMarkerIfExists(markerInformation);
        markerHashmap.put(markerInformation, marker);
    }

    @Override
    public void forceUpdatePin(TrackingData locationData) {
        HashMap<MarkerInformation, Marker> markerInfo = findMarkerByExactLocation(locationData);
        if(markerInfo != null){
            setPin(markerInfo.keySet().toArray(new MarkerInformation[0])[0]);
        }
    }

    @Nullable
    private HashMap<MarkerInformation, Marker> findMarkerByExactLocation(TrackingData locationData){
        for (final Map.Entry<MarkerInformation, Marker> entry : markerHashmap.entrySet()) {
            if (locationData.getLat() == entry.getValue().getPosition().latitude &&
                    locationData.getLon() == entry.getValue().getPosition().longitude) {
                return new HashMap<MarkerInformation, Marker>(){{
                    put(entry.getKey(), entry.getValue());
                }};
            }
        }
        return null;
    }

    @Override
    public void removeMarkerIfExists(int markerId) {
        MarkerInformation markerInfo = getMarkerInformationById(markerId);
        removeMarkerIfExists(markerInfo);
    }

    @Override
    public void removeMarkerIfExists(MarkerInformation markerInformation) {
        if (doesMarkerExist(markerInformation)) {
            Marker marker = markerHashmap.get(markerInformation);
            marker.remove();
            markerHashmap.remove(markerInformation);
        }
    }

    @Override
    public boolean doesMarkerExist(int markerId){
        MarkerInformation markerInfo = getMarkerInformationById(markerId);
        return doesMarkerExist(markerInfo);
    }

    @Override
    public boolean doesMarkerExist(MarkerInformation markerInformation){
        return markerHashmap.containsKey(markerInformation);
    }

    @Override
    public void onMarkerSelected(MarkerInformation markerInformation) {
        markerInformation.setSelected(true);
        setPin(markerInformation);
        markerInformation.adjustClickDialogPosition(getMarkerPositionXyArray(markerInformation.getId()));
        markerInformation.showMarkerDialog();
    }

    @Override
    public void onMarkerUnSelected(MarkerInformation markerInformation) {
        markerInformation.setSelected(false);
        setPin(markerInformation);
        markerInformation.hideMarkerDialog();
    }

    @Override
    public void swapMarkerSelection(MarkerInformation markerInformation){
        if(markerInformation.isSelected()){
            onMarkerUnSelected(markerInformation);
        } else {
            onMarkerSelected(markerInformation);
        }
    }

    @Override
    public void swapMarkerSelection(int markerId) {
        MarkerInformation markerInfo = getMarkerInformationById(markerId);
        if(markerInfo != null){
            swapMarkerSelection(markerInfo);
        }
    }

    @Override
    public void unSelectAllMarkers() {
        Set<MarkerInformation> markers = getNewCopyOfMarkerObjects();   //otherwise u get a concurrent modification exception
        removeAllMarkers();
        for (MarkerInformation marker : markers){
            onMarkerUnSelected(marker);
        }
    }

    private Set<MarkerInformation> getNewCopyOfMarkerObjects() {
        Set<MarkerInformation> markers = new HashSet<>();
        markers.addAll(markerHashmap.keySet());
        return markers;
    }

    @Override
    public void onMarkerSelected(int markerId) {
        MarkerInformation markerInfo = getMarkerInformationById(markerId);
        if(markerInfo != null){
            onMarkerSelected(markerInfo);
        }
    }

    @Override
    public void onMarkerUnselected(int markerId) {
        MarkerInformation markerInfo = getMarkerInformationById(markerId);
        if(markerInfo != null){
            onMarkerUnSelected(markerInfo);
        }
    }

    @Nullable
    @Override
    public MarkerInformation getMarkerInformationById(int markerId){
        Collection<MarkerInformation> markers = markerHashmap.keySet();
        for (MarkerInformation marker : markers) {
            if(marker.getId() == markerId){
                return marker;
            }
        }
        return null;
    }

    @Override
    public void removeAllMarkers() {
        for (Map.Entry<MarkerInformation, Marker> entry : markerHashmap.entrySet()) {
            Marker marker = entry.getValue();
            marker.remove();
        }
        markerHashmap.clear();
    }

    @Override
    public void removeAllMarkersExcept(int markerId) {
        Set<MarkerInformation> markers = getNewCopyOfMarkerObjects();
        removeAllMarkers();
        for (MarkerInformation marker : markers){
            if (marker.getId() == markerId) {
                setPin(marker);
            }
        }
    }

    @Override
    public void removeAllMarkersExcept(MarkerInformation markerInformation) {
        removeAllMarkersExcept(markerInformation.getId());
    }

    @Override
    public void setMapObject(GoogleMap mapObject) {
        this.googleMap = mapObject;
    }

    @Nullable
    @Override
    public int[] getMarkerPositionXyArray(int markerId){
        if(doesMarkerExist(markerId)){
            return getMarkerPositionXyArray(getMarkerInformationById(markerId));
        }
        return new int[0];
    }

    @Override
    public List<MarkerInformation> getAllMarkers() {
        return new ArrayList<>(markerHashmap.keySet());
    }

    @Override
    public int[] getMarkerPositionXyArray(MarkerInformation markerInformation){
        Point xyPoint = googleMap.getProjection().toScreenLocation(new LatLng(
                markerInformation.getTrackingData().getLat(), markerInformation.getTrackingData().getLon()));
        return new int[]{xyPoint.x, xyPoint.y};
    }
}
