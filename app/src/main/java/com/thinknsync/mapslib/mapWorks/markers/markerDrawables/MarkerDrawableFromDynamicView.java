package com.thinknsync.mapslib.mapWorks.markers.markerDrawables;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.View;

import com.thinknsync.objectwrappers.BitmapWrapper;


public class MarkerDrawableFromDynamicView implements MarkerDrawable{

    private Context context;
    private View xmlView;

    public MarkerDrawableFromDynamicView(View dynamicView) {
        this.xmlView = dynamicView;
    }

    @Override
    public Type getDrawableType() {
        return Type.VIEW;
    }

    @Override
    public BitmapWrapper getDrawableForMarker() {
        try {
            xmlView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            xmlView.layout(0, 0, xmlView.getMeasuredWidth(), xmlView.getMeasuredHeight());
            xmlView.buildDrawingCache();

            Bitmap returnedBitmap = Bitmap.createBitmap(xmlView.getMeasuredWidth(), xmlView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(returnedBitmap);
            Drawable drawable = xmlView.getBackground();
            if (drawable != null)
                drawable.draw(canvas);
            xmlView.draw(canvas);
            return new BitmapWrapper(returnedBitmap);
        } catch (Resources.NotFoundException e){
            e.printStackTrace();
        }
        return null;
    }
}
