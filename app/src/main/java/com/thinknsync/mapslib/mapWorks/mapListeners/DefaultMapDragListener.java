package com.thinknsync.mapslib.mapWorks.mapListeners;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.thinknsync.observerhost.ObserverHostImpl;

public class DefaultMapDragListener extends ObserverHostImpl implements GoogleMap.OnCameraMoveListener{
    @Override
    public void onCameraMove() {
        notifyObservers(null);
    }
}
