package com.thinknsync.mapslib.mapWorks.mapListeners;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.thinknsync.mapslib.mapWorks.markers.MarkerOperations;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.positionmanager.TrackingData;

public class DefaultMapIdleListener extends ObserverHostImpl implements GoogleMap.OnCameraIdleListener{

    @Override
    public void onCameraIdle() {
        notifyObservers(null);
    }
}
