package com.thinknsync.mapslib.mapWorks.markers.markerDrawables;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;

import com.thinknsync.objectwrappers.BitmapWrapper;


public class MarkerDrawableFromXml implements MarkerDrawable{

    private Context context;
    private int xmlId;

    public MarkerDrawableFromXml(Context context, int xmlId) {
        this.context = context;
        this.xmlId = xmlId;
    }

    @Override
    public Type getDrawableType() {
        return Type.XML;
    }

    @Override
    public BitmapWrapper getDrawableForMarker() {
        try {
            View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(xmlId, null);
            view.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());
            view.buildDrawingCache();

            Bitmap returnedBitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(returnedBitmap);
            Drawable drawable = view.getBackground();
            if (drawable != null)
                drawable.draw(canvas);
            view.draw(canvas);
            return new BitmapWrapper(returnedBitmap);
        } catch (Resources.NotFoundException e){
            e.printStackTrace();
        }
        return null;
    }
}
