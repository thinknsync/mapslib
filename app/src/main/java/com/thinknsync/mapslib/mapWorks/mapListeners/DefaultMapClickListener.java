package com.thinknsync.mapslib.mapWorks.mapListeners;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.thinknsync.mapslib.mapWorks.markers.MarkerOperations;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.positionmanager.TrackingData;

public class DefaultMapClickListener extends ObserverHostImpl<TrackingData> implements GoogleMap.OnMapClickListener {

    private MarkerOperations markerOperations;

    public DefaultMapClickListener(MarkerOperations markerOperations) {
        this.markerOperations = markerOperations;
    }

    @Override
    public void onMapClick(LatLng latLng) {
        markerOperations.unSelectAllMarkers();
        notifyObservers(new TrackingData(latLng.latitude, latLng.longitude));
    }
}
