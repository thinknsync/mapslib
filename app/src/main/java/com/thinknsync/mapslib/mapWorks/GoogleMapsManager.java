package com.thinknsync.mapslib.mapWorks;

import com.google.android.gms.maps.GoogleMap;
import com.thinknsync.apilib.BaseResponseActions;
import com.thinknsync.logger.AndroidLogger;
import com.thinknsync.logger.Logger;
import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.mapslib.Units;
import com.thinknsync.mapslib.apiCall.ApiCallFields;
import com.thinknsync.mapslib.mapWorks.mapListeners.DefaultMapClickListener;
import com.thinknsync.mapslib.mapWorks.mapListeners.DefaultMapDragListener;
import com.thinknsync.mapslib.mapWorks.mapListeners.DefaultMapIdleListener;
import com.thinknsync.mapslib.mapWorks.mapListeners.DefaultMapMoveStartListener;
import com.thinknsync.mapslib.mapWorks.mapListeners.DefaultMarkerClickListener;
import com.thinknsync.mapslib.mapWorks.mapListeners.MapCameraIdleCenterPlaceFetcher;
import com.thinknsync.mapslib.mapWorks.markers.MarkerInformation;
import com.thinknsync.mapslib.mapWorks.markers.MarkerOperations;
import com.thinknsync.mapslib.mapWorks.markers.MarkerOperationsImpl;
import com.thinknsync.mapslib.mapsDrawer.GoogleMapsPathDataGetter;
import com.thinknsync.mapslib.mapsDrawer.PathDataFetcher;
import com.thinknsync.mapslib.mapsDrawer.PathDataManager;
import com.thinknsync.mapslib.mapsDrawer.PathDataManagerImpl;
import com.thinknsync.mapslib.mapsDrawer.PathProperties;
import com.thinknsync.mapslib.place.PlaceDataObject;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.TrackingData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by shuaib on 5/20/17.
 */

public class GoogleMapsManager implements MapsManager<GoogleMap> {

    private Logger logger;
    private MapsOperation<GoogleMap> googleMapsOperation;
    private MarkerOperations<GoogleMap> mapMarkerOperations;
    private PathDataManager pathData;
    private AndroidContextWrapper contextWrapper;

    public GoogleMapsManager(MapsOperation<GoogleMap> mapsOperation, AndroidContextWrapper context) {
        this.googleMapsOperation = mapsOperation;
        this.contextWrapper = context;
        this.mapMarkerOperations = new MarkerOperationsImpl(mapsOperation.getMap());
        initMapDefaultListeners();

        logger = AndroidLogger.getInstance();
    }

    private void initMapDefaultListeners() {
        setOnMapClickAction();
        setOnMarkerClickAction();
        setCameraMoveStartListener();
        setCameraIdleListener();
        setDragListener();
    }

    @Override
    public void drawPath(final TrackingData[] sourceAndDestination, final PathDataFetcher.NavigationMode navigationMode,
                         final TypedObserver<Integer> resultCallback, final PathProperties... pathProperties) {
        PathDataFetcher dataFetcher = new GoogleMapsPathDataGetter(contextWrapper, sourceAndDestination, navigationMode);
        dataFetcher.getRouteData(new BaseResponseActions() {
            @Override
            public void onApiCallSuccess(int reqId, JSONObject responseJson) throws JSONException {
                logger.debugLog("route data success", responseJson.toString());
                pathData = new PathDataManagerImpl(responseJson, Units.KM, TimeUnit.MINUTES);
//                double totalDistanceKms = pathData.getPathDistanceInMeters() / 1000;
//                int totalTimeMins = pathData.getCommuteTimeInSecs() / 60;
//                logger.debugLog("distance and time", String.valueOf(totalDistanceKms) +" and " + String.valueOf(totalTimeMins));
                int pathId = googleMapsOperation.drawPath(pathData, pathProperties);
                resultCallback.update(pathId);
            }

            @Override
            public void onApiCallFailure(int reqId, JSONObject responseJson) throws JSONException {
                logger.debugLog("route data failure", responseJson.toString());
                if(responseJson.getString(ApiCallFields.GoogleApiResponseFields.KEY_STATUS).equalsIgnoreCase(ApiCallFields.GoogleApiResponseFields.VALUE_OVER_LIMIT)){
                    drawPath(sourceAndDestination, navigationMode, resultCallback);
                }
            }
        });
    }

    @Override
    public void setZoomLevel(TrackingData[] locationData){
        googleMapsOperation.setZoomLevel(locationData);
    }

    @Override
    public void setZoomLevel(TrackingData[] locationDataList, int[] paddingLeftTopRightBottom) {
        googleMapsOperation.setZoomLevel(locationDataList, paddingLeftTopRightBottom);
    }

    @Override
    public void setZoomLevel(int zoomLevel) {
        googleMapsOperation.setZoomLevel(zoomLevel);
    }

    @Override
    public void setShouldGoToCurrentLocation(boolean shouldGoToCurrentLocation) {
        googleMapsOperation.setShouldGoToCurrentLocation(shouldGoToCurrentLocation);
    }

    @Override
    public GoogleMap getMap() {
        return googleMapsOperation.getMap();
    }

    @Override
    public float getDefaultZoomLevel() {
        return googleMapsOperation.getDefaultZoomLevel();
    }

    @Override
    public int drawPath(PathDataManager pathData, PathProperties... pathProperties) {
        return googleMapsOperation.drawPath(pathData, pathProperties);
    }

    @Override
    public void drawPath(List<TrackingData> trackingDataList, PathProperties... pathProperties) {
        googleMapsOperation.drawPath(trackingDataList, pathProperties);
    }

    @Override
    public void clearPath(int pathId) {
        googleMapsOperation.clearPath(pathId);
    }

    @Override
    public void clearAllPaths() {
        googleMapsOperation.clearAllPaths();
    }


    @Override
    public void setCameraMoveStartListener(TypedObserver... moveStartListener) {
        ObserverHost observerHost = getMapMoveStartObserverHost();
        observerHost.addToObservers(Arrays.asList(moveStartListener));
        setupMapMoveStartListener(observerHost);
    }

    @Override
    public void setupMapMoveStartListener(ObserverHost moveStartObserverHost) {
        googleMapsOperation.setupMapMoveStartListener(moveStartObserverHost);

    }

    @Override
    public void setCameraIdleListener(TypedObserver... cameraIdleObserver) {
        ObserverHost mapIdleObservable = getMapIdleObserverHost();
        mapIdleObservable.addToObservers(Arrays.asList(cameraIdleObserver));
        setupMapIdleCenterListener(mapIdleObservable);
    }

    @Override
    public void setupMapIdleCenterListener(ObserverHost centerCoordinateFetcher) {
        googleMapsOperation.setupMapIdleCenterListener(centerCoordinateFetcher);
    }

    @Override
    public void setObserverToCenterCoordinateFetch(TypedObserver<List<PlaceDataObject>>... cameraIdleObserver) {
        ObserverHost<List<PlaceDataObject>> centerCoordinateFetcher = new MapCameraIdleCenterPlaceFetcher(contextWrapper, googleMapsOperation);
        centerCoordinateFetcher.addToObservers(Arrays.asList(cameraIdleObserver));
        setCameraIdleListener((TypedObserver)centerCoordinateFetcher);
    }

    @Override
    public void setDragListener(TypedObserver... cameraMoveObserver) {
        ObserverHost observerHost = getMapDragObserverHost();
        observerHost.addToObservers(Arrays.asList(cameraMoveObserver));
        setupDragListener(observerHost);
    }

    @Override
    public void setupDragListener(ObserverHost cameraMoveObserver) {
        googleMapsOperation.setupDragListener(cameraMoveObserver);
    }

    @Override
    public void setOnMarkerClickAction(final TypedObserver<MarkerInformation>... markerClickObserver) {
        ObserverHost<MarkerInformation> markerClickObservable = getMapMarkerClickObserverHost();
        markerClickObservable.addToObservers(Arrays.asList(markerClickObserver));
        setOnMarkerClickAction(markerClickObservable);
    }

    @Override
    public void setOnMarkerClickAction(ObserverHost<MarkerInformation> markerClickObserver) {
        googleMapsOperation.setOnMarkerClickAction(markerClickObserver);
    }

    @Override
    public void setOnMapClickAction(TypedObserver<TrackingData>... clickObserver) {
        ObserverHost<TrackingData> mapClickObservable = getMapClickObserverHost();
        mapClickObservable.addToObservers(Arrays.asList(clickObserver));
        setOnMapClickAction(mapClickObservable);
    }

    @Override
    public void setOnMapClickAction(ObserverHost<TrackingData> clickObserver) {
        googleMapsOperation.setOnMapClickAction(clickObserver);
    }

    @Override
    public void resetCameraMoveStartListener() {
        getMapMoveStartObserverHost().clearObservers();
    }

    @Override
    public void resetCameraIdleListener() {
        getMapIdleObserverHost().clearObservers();
    }

    @Override
    public void resetDragListener() {
        getMapIdleObserverHost().clearObservers();
    }

    @Override
    public void resetOnMarkerClickAction() {
        getMapIdleObserverHost();
    }

    @Override
    public void resetOnMapClickAction() {
        getMapIdleObserverHost().clearObservers();
    }

    @Override
    public void resetObserverToCenterCoordinateFetch() {
        getMapIdleObserverHost().clearObservers();
    }

    @Override
    public boolean isLocationInScreen(TrackingData trackingData) {
        return googleMapsOperation.isLocationInScreen(trackingData);
    }

    @Override
    public boolean isMapMoving() {
        return googleMapsOperation.isMapMoving();
    }

    @Override
    public ObserverHost<TrackingData> getMapClickObserverHost() {
        return googleMapsOperation.getMapClickObserverHost() != null ?
                googleMapsOperation.getMapClickObserverHost() : new DefaultMapClickListener(mapMarkerOperations);
    }

    @Override
    public ObserverHost getMapDragObserverHost() {
        return googleMapsOperation.getMapDragObserverHost() != null ?
                googleMapsOperation.getMapDragObserverHost() : new DefaultMapDragListener();
    }

    @Override
    public ObserverHost<MarkerInformation> getMapMarkerClickObserverHost() {
        return googleMapsOperation.getMapMarkerClickObserverHost() != null ?
                googleMapsOperation.getMapMarkerClickObserverHost(): new DefaultMarkerClickListener(mapMarkerOperations, googleMapsOperation);
    }

    @Override
    public ObserverHost getMapIdleObserverHost() {
        return googleMapsOperation.getMapIdleObserverHost() != null ?
                googleMapsOperation.getMapIdleObserverHost() : new DefaultMapIdleListener();
    }

    @Override
    public ObserverHost getMapMoveStartObserverHost() {
        return googleMapsOperation.getMapMoveStartObserverHost() != null ?
                googleMapsOperation.getMapMoveStartObserverHost() : new DefaultMapMoveStartListener();
    }

    @Override
    public void clearMap(){
        googleMapsOperation.clearMap();
        removeAllMarkers();
    }

    @Override
    public void setMapView(TrackingData locationData, float zoomLevel) {
        googleMapsOperation.setMapView(locationData, zoomLevel);
    }

    @Override
    public void setMapView(TrackingData locationData) {
        googleMapsOperation.setMapView(locationData);
    }

    @Override
    public void setMapView(TrackingData[] locationDataList) {
        googleMapsOperation.setMapView(locationDataList);
    }

    @Override
    public PathDataManager getPathData(){
        return pathData;
    }

    @Override
    public void enableDisableMapInteraction(boolean enableDisable) {
        googleMapsOperation.enableDisableMapInteraction(enableDisable);
    }

    @Override
    public void unSelectAllMarkers() {
        mapMarkerOperations.unSelectAllMarkers();
    }

    @Override
    public void onMarkerSelected(final int integer) {
        setCameraIdleListener(new TypedObserverImpl<Object>() {
            @Override
            public void update(Object o) {
                mapMarkerOperations.onMarkerSelected(integer);
            }

            public void update(ObserverHost<Object> self, Object o) {
                self.removeObserver(this);
                super.update(self, o);
            }
        });
//        mapMarkerOperations.onMarkerSelected(integer);
    }

    @Override
    public void onMarkerUnselected(int integer) {
        mapMarkerOperations.onMarkerUnselected(integer);
    }

    @Override
    public int[] getMarkerPositionXyArray(MarkerInformation markerInformation) {
        return mapMarkerOperations.getMarkerPositionXyArray(markerInformation);
    }

    @Override
    public int[] getMarkerPositionXyArray(int markerId) {
        return mapMarkerOperations.getMarkerPositionXyArray(markerId);
    }

    @Override
    public List<MarkerInformation> getAllMarkers() {
        return mapMarkerOperations.getAllMarkers();
    }

    @Override
    public void setPin(MarkerInformation markerInformation) {
        mapMarkerOperations.setPin(markerInformation);
    }

    @Override
    public void forceUpdatePin(TrackingData locationData) {
        mapMarkerOperations.forceUpdatePin(locationData);
    }

    @Override
    public MarkerInformation getMarkerInformationById(int markerId) {
        return mapMarkerOperations.getMarkerInformationById(markerId);
    }

    @Override
    public void removeMarkerIfExists(int markerId) throws ConcurrentModificationException {
        mapMarkerOperations.removeMarkerIfExists(markerId);
    }

    @Override
    public void removeMarkerIfExists(MarkerInformation markerInformation) throws ConcurrentModificationException{
        mapMarkerOperations.removeMarkerIfExists(markerInformation);
    }

    @Override
    public void removeAllMarkers() {
        mapMarkerOperations.removeAllMarkers();
    }

    @Override
    public void removeAllMarkersExcept(int markerId) {
        mapMarkerOperations.removeAllMarkersExcept(markerId);
    }

    @Override
    public void removeAllMarkersExcept(MarkerInformation markerInformation) {
        mapMarkerOperations.removeAllMarkersExcept(markerInformation);
    }

    @Override
    public void setMapObject(GoogleMap mapObject) {
        mapMarkerOperations.setMapObject(mapObject);
    }

    @Override
    public boolean doesMarkerExist(int markerId) {
        return mapMarkerOperations.doesMarkerExist(markerId);
    }

    @Override
    public boolean doesMarkerExist(MarkerInformation markerInformation) {
        return mapMarkerOperations.doesMarkerExist(markerInformation);
    }

    @Override
    public void onMarkerSelected(final MarkerInformation markerInformation) {
        setCameraIdleListener(new TypedObserverImpl<Object>() {
            @Override
            public void update(Object o) {
                mapMarkerOperations.onMarkerSelected(markerInformation);
            }

            @Override
            public void update(ObserverHost<Object> self, Object o) {
                self.removeObserver(this);
                super.update(self, o);
            }
        });
    }

    @Override
    public void onMarkerUnSelected(MarkerInformation markerInformation) {
        mapMarkerOperations.onMarkerUnSelected(markerInformation);
    }

    @Override
    public void swapMarkerSelection(MarkerInformation markerInformation) {
        mapMarkerOperations.swapMarkerSelection(markerInformation);
    }

    @Override
    public void swapMarkerSelection(int markerId) {
        mapMarkerOperations.swapMarkerSelection(markerId);
    }
}
