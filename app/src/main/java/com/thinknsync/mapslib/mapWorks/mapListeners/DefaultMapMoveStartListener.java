package com.thinknsync.mapslib.mapWorks.mapListeners;

import com.google.android.gms.maps.GoogleMap;
import com.thinknsync.observerhost.ObserverHostImpl;

public class DefaultMapMoveStartListener extends ObserverHostImpl implements GoogleMap.OnCameraMoveStartedListener{

    @Override
    public void onCameraMoveStarted(int i) {
        notifyObservers(null);
    }
}
