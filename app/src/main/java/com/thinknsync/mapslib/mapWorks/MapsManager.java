package com.thinknsync.mapslib.mapWorks;

import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.mapslib.mapWorks.markers.MarkerInformation;
import com.thinknsync.mapslib.mapWorks.markers.MarkerOperations;
import com.thinknsync.mapslib.mapsDrawer.PathDataFetcher;
import com.thinknsync.mapslib.mapsDrawer.PathDataManager;
import com.thinknsync.mapslib.mapsDrawer.PathProperties;
import com.thinknsync.mapslib.place.PlaceDataObject;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.positionmanager.TrackingData;

import java.util.List;

/**
 * Created by shuaib on 5/20/17.
 */

public interface MapsManager<T> extends MapsOperation<T>, MarkerOperations<T>{
    void drawPath(TrackingData[] sourceAndDestination, PathDataFetcher.NavigationMode navigationMode,
                  TypedObserver<Integer> resultCallback, PathProperties... pathProperties);
    void setCameraMoveStartListener(TypedObserver... moveStartListener);
    void setCameraIdleListener(TypedObserver... cameraIdleObserver);
    void setDragListener(TypedObserver... cameraMoveObserver);
    void setOnMarkerClickAction(TypedObserver<MarkerInformation>... markerClickObserver);
    void setOnMapClickAction(TypedObserver<TrackingData>... clickObserver);
    void setObserverToCenterCoordinateFetch(TypedObserver<List<PlaceDataObject>>... cameraIdleObserver);
    void resetCameraMoveStartListener();
    void resetCameraIdleListener();
    void resetDragListener();
    void resetOnMarkerClickAction();
    void resetOnMapClickAction();
    void resetObserverToCenterCoordinateFetch();
    PathDataManager getPathData();
}
