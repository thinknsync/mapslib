package com.thinknsync.mapslib.mapWorks.markers.markerDrawables;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;

import com.thinknsync.objectwrappers.BitmapWrapper;

public class MarkerDrawableFromDrawableId implements MarkerDrawable {

    private Context context;
    private int drawableId;

    public MarkerDrawableFromDrawableId(Context context, int drawableId) {
        this.context = context;
        this.drawableId = drawableId;
    }

    @Override
    public Type getDrawableType() {
        return Type.DRAWABLE;
    }

    @Override
    public BitmapWrapper getDrawableForMarker() {
        try {
            Drawable drawable = context.getResources().getDrawable(drawableId);
            Bitmap bmp = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            Bitmap smallMarker = Bitmap.createScaledBitmap(bmp, 150, 150, false);
            final Canvas canvas = new Canvas(smallMarker);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
            return new BitmapWrapper(smallMarker);
        } catch (Resources.NotFoundException e){
            e.printStackTrace();
        }
        return null;

    }
}
