package com.thinknsync.mapslib.mapWorks.markers.markerDrawables;

import com.thinknsync.objectwrappers.FrameworkWrapper;

public interface MarkerDrawable {
    Type getDrawableType();
    FrameworkWrapper getDrawableForMarker();

    enum Type {
        VIEW, XML, DRAWABLE;
    }
}
