package com.thinknsync.mapslib.mapWorks.mapListeners;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.thinknsync.logger.AndroidLogger;
import com.thinknsync.logger.Logger;
import com.thinknsync.mapslib.MapsOperation;
import com.thinknsync.mapslib.place.PlaceHelper;
import com.thinknsync.mapslib.place.GooglePlaceHelper;
import com.thinknsync.mapslib.place.PlaceDataObject;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;
import com.thinknsync.positionmanager.TrackingData;

import java.util.List;


public class MapCameraIdleCenterPlaceFetcher extends ObserverHostImpl<List<PlaceDataObject>> implements TypedObserver<Void> {

    private MapsOperation<GoogleMap> mapOperation;
    private PlaceHelper placeHelper;
    private Logger logger;

    public MapCameraIdleCenterPlaceFetcher(AndroidContextWrapper contextWrapper, MapsOperation googleMap) {
        this.mapOperation = googleMap;
        this.logger = AndroidLogger.getInstance();
        this.placeHelper = new GooglePlaceHelper(contextWrapper, logger);
        placeHelper.addToObservers(new TypedObserverImpl<List<PlaceDataObject>>() {
            @Override
            public void update(List<PlaceDataObject> placeDataObjects) {
                notifyObservers(placeDataObjects);
            }
        });
    }

    private TrackingData getLocationDataFromMaps() {
        LatLng latLng = mapOperation.getMap().getCameraPosition().target;
        TrackingData locationData = new TrackingData(latLng.latitude, latLng.longitude);
        return locationData;
    }

    @Override
    public void onOperationError(ObserverHost observerHost, Exception e) {
        e.printStackTrace();
    }

    @Override
    public void update(Void nothing) {
        try {
            TrackingData locationData = getLocationDataFromMaps();
            logger.debugLog("center location", String.valueOf(locationData.getLat()) + ", " + String.valueOf(locationData.getLon()));
            placeHelper.guessPlaceFromCoordiante(locationData);
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(ObserverHost<Void> observerHost, Void nothing) {
        update(nothing);
    }
}
