package com.thinknsync.mapslib.mapWorks.markers;

import com.thinknsync.objectwrappers.DialogWrapper;
import com.thinknsync.observerhost.TypedObserver;

public interface MarketInfoDialog<T> extends DialogWrapper<T> {
    void onDismiss(TypedObserver observer);
    void adjustDialogPosition(int[] xyArray);
}
