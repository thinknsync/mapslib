package com.thinknsync.mapslib.place;

import com.thinknsync.positionmanager.TrackingData;

/**
 * Created by shuaib on 2/27/18.
 */

public interface PlaceDataObject {
    String key_place_id = "placeId";
    String key_place_title = "placeTitle";
    String key_place_subtitle = "placeSubtitle";
    String key_place_lat = "placeLat";
    String key_place_lon = "placeLon";
    String key_place_image = "placeImage";

    String getPlaceTitle();
    void setPlaceTitle(String title);
    void setLon(double longitude);
    void setLat(double latitude);
    double getLat();
    double getLon();
    TrackingData getPlaceLocation();
    String getPlaceId();
    void setPlaceId(String placeId);
    String getPlaceSubtitle();
    void setPlaceSubtitle(String placeSubtitle);
    int getImage();
    void setImage(int image);
}
