package com.thinknsync.mapslib.place;


import com.thinknsync.convertible.BaseConvertible;
import com.thinknsync.mapslib.place.PlaceDataObject;
import com.thinknsync.positionmanager.TrackingData;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

/**
 * Created by shuaib on 5/23/17.
 */

public class GooglePlaceDataObject extends BaseConvertible<PlaceDataObject> implements PlaceDataObject{

    private String placeId;
    private String placeTitle;
    private String placeSubtitle;
    private TrackingData trackingData;
    private int image;

    public GooglePlaceDataObject(){
        trackingData = new TrackingData();
    }

    public GooglePlaceDataObject(String name, double lat, double lon){
        this.placeTitle = name;
        this.trackingData = new TrackingData(lat, lon);
    }

    public GooglePlaceDataObject(String placeId, String placeTitle, String placeSubitle) {
        this.placeId = placeId;
        this.placeTitle = placeTitle;
        this.placeSubtitle = placeSubitle;
        this.trackingData = new TrackingData();
    }

    @Override
    public String getPlaceId() {
        return placeId;
    }

    @Override
    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    @Override
    public String getPlaceTitle() {
        return placeTitle;
    }

    @Override
    public void setPlaceTitle(String placeTitle) {
        this.placeTitle = placeTitle;
    }

    @Override
    public String getPlaceSubtitle() {
        return placeSubtitle;
    }

    @Override
    public void setPlaceSubtitle(String placeSubtitle) {
        this.placeSubtitle = placeSubtitle;
    }

    @Override
    public double getLat() {
        return trackingData.getLat();
    }

    @Override
    public void setLat(double lat) {
        trackingData.setLat(lat);
    }

    @Override
    public double getLon() {
        return trackingData.getLon();
    }

    @Override
    public void setLon(double lon) {
        trackingData.setLon(lon);
    }

    @Override
    public TrackingData getPlaceLocation(){
        return trackingData;
    }

    @Override
    public int getImage() {
        return image;
    }

    @Override
    public void setImage(int image) {
        this.image = image;
    }

    @Override
    public PlaceDataObject fromJson(JSONObject jsonObject) throws JSONException {
        setPlaceId(jsonObject.optString(key_place_id));
        setPlaceTitle(jsonObject.getString(key_place_title));
        setPlaceSubtitle(jsonObject.optString(key_place_subtitle));
        setLat(jsonObject.optDouble(key_place_lat));
        setLon(jsonObject.optDouble(key_place_lon));
        setImage(jsonObject.optInt((key_place_image)));
        return this;
    }

    @Override
    public PlaceDataObject fromMap(Map objectMap) {
        setPlaceId((String)objectMap.get(key_place_id));
        setPlaceTitle((String) objectMap.get(key_place_title));
        setPlaceSubtitle((String) objectMap.get(key_place_subtitle));
        setLat((double) objectMap.get(key_place_lat));
        setLon((double) objectMap.get(key_place_lon));
        setImage((int) objectMap.get(key_place_image));
        return this;
    }

    @Override
    public JSONObject toJson() throws JSONException {
        return new JSONObject().put(key_place_id, getPlaceId())
                .put(key_place_title, getPlaceTitle())
                .put(key_place_subtitle, getPlaceSubtitle())
                .put(key_place_lat, getLat())
                .put(key_place_lon, getLon())
                .put(key_place_image, getImage());
    }
}
