package com.thinknsync.mapslib.place;

import androidx.annotation.NonNull;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.thinknsync.logger.AndroidLogger;
import com.thinknsync.logger.Logger;
import com.thinknsync.objectwrappers.AndroidContextWrapper;

import java.util.ArrayList;
import java.util.List;

public class GoogleAutoCompleteHelper extends GooglePlaceClientContainer implements GetLocationFromString {

//    private final RectangularBounds bounds = RectangularBounds.newInstance(
//            new LatLng(22.967459, 89.580246), //southWest, close to khulna
//            new LatLng(24.856292, 91.785176)); //northEast, close to sylhet);

    private AutocompleteSessionToken token;

    public GoogleAutoCompleteHelper(AndroidContextWrapper contextWrapper) {
        super(contextWrapper, AndroidLogger.getInstance());
        token = AutocompleteSessionToken.newInstance();
    }

    @Override
    public void findPlace(String searchString) {
        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                .setCountry("bd")
                .setSessionToken(token)
                .setQuery(searchString)
                .build();

        placesClient.findAutocompletePredictions(request).addOnSuccessListener(
                new OnSuccessListener<FindAutocompletePredictionsResponse>() {
            @Override
            public void onSuccess(FindAutocompletePredictionsResponse response) {
                List<PlaceDataObject> autoCompleteResults = getAutoCompleteResult(response.getAutocompletePredictions());
                notifyObservers(autoCompleteResults);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                if (exception instanceof ApiException) {
                    ApiException apiException = (ApiException) exception;
                    logger.errorLog("Place not found: ", String.valueOf(apiException.getStatusCode()));
                }
            }
        });
    }

    private List<PlaceDataObject> getAutoCompleteResult(List<AutocompletePrediction> autocompletePredictions) {
        List<PlaceDataObject> autoCompleteResults = new ArrayList<>();
        for(AutocompletePrediction prediction: autocompletePredictions){
            logger.debugLog("autocomplete properties", "placeId: " + prediction.getPlaceId() +
                    "-" + "placeFullText: " + prediction.getFullText(null).toString() + "-placeSecondaryText: " +
                    prediction.getSecondaryText(null).toString() + "-placePrimaryText: " + prediction.getPrimaryText(null) +
                    "-placeTypes: " + prediction.getPlaceTypes());
            PlaceDataObject placeDataObject = new GooglePlaceDataObject(prediction.getPlaceId(), prediction.getPrimaryText(null).toString(),
                    prediction.getSecondaryText(null).toString());
            autoCompleteResults.add(placeDataObject);
        }

        return autoCompleteResults;
    }
}
